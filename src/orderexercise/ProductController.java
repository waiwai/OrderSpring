package orderexercise;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ProductController {
	@Autowired
	ProductDAL productDal;
	List<Product> productList;
	ArrayList<String> row;
	
	
	 
	@RequestMapping(value = "/")
	public ModelAndView maiin() {
		return new ModelAndView("redirect:/product");
	}

	@RequestMapping(value = "/product", method = RequestMethod.GET)
	public ModelAndView product(@RequestParam(required = false) Integer page) {
		productList = new ArrayList<>();
		try {
			productList = productDal.getAll();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		PagedListHolder<Product> pagedListHolder = new PagedListHolder<>(productList);
		pagedListHolder.setPageSize(5);

		ModelAndView modelView = new ModelAndView("product");

		modelView.addObject("maxPages", pagedListHolder.getPageCount());
		
		if (page == null || page < 1 || page > pagedListHolder.getPageCount())
			page = 1;
		modelView.addObject("page", page);
		
		if (page == null || page < 1 || page > pagedListHolder.getPageCount()) {
			pagedListHolder.setPage(0);
			modelView.addObject("productList", pagedListHolder.getPageList());
		} else if (page <= pagedListHolder.getPageCount()) {
			pagedListHolder.setPage(page - 1);
			modelView.addObject("productList", pagedListHolder.getPageList());
		}

		return modelView;
	}

	@RequestMapping(value = "/addproduct", method = RequestMethod.GET)
	public ModelAndView addProduct() {

		ModelAndView modelView = new ModelAndView("addproduct", "product", new Product());
		return modelView;
	}

	@RequestMapping(value = "/addproduct", method = RequestMethod.POST)
	public ModelAndView addproduct(@ModelAttribute("product") @Valid Product product, BindingResult result, ModelMap model) {
		
		if(result.hasErrors()) {			
			return new ModelAndView("addproduct");
        }
		
		else{
			try {
			productDal.add(product);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		}
		return new ModelAndView("redirect:/product");
	
	}
	

	@RequestMapping(value = "/deleteproduct", method = RequestMethod.GET)
	public ModelAndView deleteProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {
		int id = Integer.parseInt(request.getParameter("id"));
		Product product = findProductById(id);
		productDal.delete(product);
		return new ModelAndView("redirect:/product");
	}

	@RequestMapping(value = "/editproduct", method = RequestMethod.POST)
	public ModelAndView editproduct(@ModelAttribute("SpringWeb") Product product, ModelMap model) {

		try {
			productDal.update(product);
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}
		return new ModelAndView("redirect:/product");
	}

	@RequestMapping(value = "/editproduct", method = RequestMethod.GET)
	public ModelAndView editProduct(HttpServletRequest request, HttpServletResponse response) throws Exception {

		int id = Integer.parseInt(request.getParameter("id"));

		Product product = findProductById(id);

		ModelAndView modelView = new ModelAndView("editproduct", "command", product);
		return modelView;
	}

	@RequestMapping(value = "/downloadCSV", method = RequestMethod.GET)
	public void doDownload(HttpServletRequest request, HttpServletResponse response) throws IOException {

		String csvFileName = "product.csv";
		response.setContentType("text/csv");
		String headerKey = "Content-Disposition";
		String headerValue = String.format("attachment; filename=\"%s\"", csvFileName);

		// get absolute path of the application
		ServletContext context = request.getServletContext();
		String appPath = context.getRealPath("");
		System.out.println("appPath = " + appPath);
		response.setHeader(headerKey, headerValue);

		productList = new ArrayList<>();
		try {
			productList = productDal.getAll();
		} catch (ClassNotFoundException | SQLException e) {
			e.printStackTrace();
		}

		row = new ArrayList<>();
		for (Product p : productList) {
			row.add(String.valueOf(p.getId()));
			row.add(",");
			row.add(p.getName());
			row.add(",");
			row.add(String.valueOf(p.getPrice()));
			row.add("\n");
		}

		Iterator<String> iterator = row.iterator();
		while (iterator.hasNext()) {
			String datas = iterator.next();
			response.getOutputStream().print(datas);
			;
		}
		response.getOutputStream().flush();
	}

	private Product findProductById(int id) {
		Product product = new Product();
		for (Product p : productList) {
			if (p.getId() == id) {
				product.setId(p.getId());
				product.setName(p.getName());
				product.setPrice(p.getPrice());
			}
		}
		return product;
	}
	
	
}
